let http = require('http')
let port = 4000

let server = http.createServer(function(request, response) {

	if(request.url == '/profile'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile!')
	} else if (request.url == '/courses'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end(`Here's our courses available`)
	} else if (request.url == '/addcourse'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add a course to our resources')
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
	}
})

server.listen(port)
console.log(`Server is running on localhost: ${port}`)